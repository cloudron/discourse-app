#!/bin/bash

set -eu

cd /app/code

# do not create public directory here, since code below depends on it
echo "==> Creating directories"
mkdir -p /run/discourse/log /run/discourse/tmp /run/nginx/cache /app/data/uploads /app/data/plugins /run/home-dotconfig /run/gem-spec-cache /run/cloudron.local
touch /run/cloudron.irb_history

export XDG_CONFIG_HOME=/run/home-dotconfig # ember-cli stores something in ~/.config
export GEM_SPEC_CACHE=/run/gem-spec-cache  # gem environment

echo "==> Configuring discourse"
sed -e "s/db_host.*=.*/db_host = ${CLOUDRON_POSTGRESQL_HOST}/" \
    -e "s/db_port.*=.*/db_port = ${CLOUDRON_POSTGRESQL_PORT}/" \
    -e "s/db_name.*=.*/db_name = ${CLOUDRON_POSTGRESQL_DATABASE}/" \
    -e "s/db_username.*=.*/db_username = ${CLOUDRON_POSTGRESQL_USERNAME}/" \
    -e "s/db_password.*=.*/db_password = ${CLOUDRON_POSTGRESQL_PASSWORD}/" \
    -e "s/smtp_address.*=.*/smtp_address = ${CLOUDRON_MAIL_SMTP_SERVER}/" \
    -e "s/smtp_port.*=.*/smtp_port = ${CLOUDRON_MAIL_SMTP_PORT}/" \
    -e "s/smtp_domain.*=.*/smtp_domain = ${CLOUDRON_MAIL_DOMAIN}/" \
    -e "s/smtp_user_name.*=.*/smtp_user_name = ${CLOUDRON_MAIL_SMTP_USERNAME}/" \
    -e "s/smtp_password.*=.*/smtp_password = ${CLOUDRON_MAIL_SMTP_PASSWORD}/" \
    -e "s/hostname.*=.*/hostname = ${CLOUDRON_APP_DOMAIN}/" \
    -e "s/redis_host.*=.*/redis_host= ${CLOUDRON_REDIS_HOST}/" \
    -e "s/redis_port*=.*/redis_port = ${CLOUDRON_REDIS_PORT}/" \
    -e "s/redis_password.*=.*/redis_password = ${CLOUDRON_REDIS_PASSWORD}/" \
    -e "s/new_version_emails.*=.*/new_version_emails = false/" \
    -e "s/serve_static_assets.*=.*/serve_static_assets = true/" \
    -e "s/developer_emails.*=.*/developer_emails = test@cloudron.io/" \
    -e "s/refresh_maxmind_db_during_precompile_days.*=.*/refresh_maxmind_db_during_precompile_days = 0/" \
    /app/code/config/discourse_defaults.conf > /run/discourse/discourse.conf

# plugins
echo "==> Creating symlinks for built-in plugins"
for plugin in `find "/app/code/plugins.original"/* -maxdepth 0 -type d -printf "%f\n"`; do
    rm -f /app/data/plugins/${plugin}
    ln -sf /app/code/plugins.original/${plugin} /app/data/plugins/${plugin}
done

# create dummy settings file
if [[ ! -f /app/data/site_settings.yml ]]; then
    echo "# Add additional customizations in this file" > /app/data/site_settings.yml
fi

# merge user yaml file (yaml does not allow key re-declaration)
cp /app/code/config/site_settings.yml.default /run/discourse/site_settings.yml
if [[ -n "${CLOUDRON_MAIL_IMAP_SERVER:-}" ]]; then
    reply_to_address=$(echo "${CLOUDRON_MAIL_IMAP_USERNAME}" | sed -e 's/\(.*\)@/\1+%{reply_key}@/')

    echo "==> Enabling recvmail integration"
    yq eval -i ".email.reply_by_email_enabled=true" /run/discourse/site_settings.yml
    yq eval -i ".email.reply_by_email_address=\"${reply_to_address}\"" /run/discourse/site_settings.yml
    yq eval -i ".email.pop3_polling_enabled=true" /run/discourse/site_settings.yml
    yq eval -i ".email.pop3_polling_ssl=false" /run/discourse/site_settings.yml
    yq eval -i ".email.pop3_polling_openssl_verify=false" /run/discourse/site_settings.yml
    yq eval -i ".email.pop3_polling_period_mins=5" /run/discourse/site_settings.yml
    yq eval -i ".email.pop3_polling_host=\"${CLOUDRON_MAIL_IMAP_SERVER}\"" /run/discourse/site_settings.yml
    yq eval -i ".email.pop3_polling_port=${CLOUDRON_MAIL_POP3_PORT}" /run/discourse/site_settings.yml
    yq eval -i ".email.pop3_polling_username=\"${CLOUDRON_MAIL_IMAP_USERNAME}\"" /run/discourse/site_settings.yml
    yq eval -i ".email.pop3_polling_password.default=\"${CLOUDRON_MAIL_IMAP_PASSWORD}\"" /run/discourse/site_settings.yml
    yq eval -i ".email.pop3_polling_delete_from_server=true" /run/discourse/site_settings.yml
fi
/app/pkg/yaml-override.js /run/discourse/site_settings.yml /app/data/site_settings.yml

echo "==> Changing permissions"
chown -R cloudron:cloudron /run/discourse /app/data /run/home-dotconfig /run/gem-spec-cache /run/cloudron.local
# runtime dirs
chown -R cloudron:cloudron /app/code/public /app/code/app/assets/javascripts/plugins /app/code/app/assets/javascripts/discourse/{dist,node_modules}

# PATH is set so that it can find svgo
# this also activates plugins which generates assets into app/assets/javascripts/plugins
echo "==> Migrating database"
gosu cloudron:cloudron bundle exec rake db:migrate

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    echo "==> Setup OpenID connect integration"
    # CLOUDRON_OIDC_PROVIDER_NAME is not supported
    gosu cloudron:cloudron bundle exec script/rails runner "SiteSetting.openid_connect_enabled = true"
    gosu cloudron:cloudron bundle exec script/rails runner "SiteSetting.openid_connect_client_id = '${CLOUDRON_OIDC_CLIENT_ID}'"
    gosu cloudron:cloudron bundle exec script/rails runner "SiteSetting.openid_connect_client_secret = '${CLOUDRON_OIDC_CLIENT_SECRET}'"
    gosu cloudron:cloudron bundle exec script/rails runner "SiteSetting.openid_connect_authorize_scope = 'openid email profile'"
    gosu cloudron:cloudron bundle exec script/rails runner "SiteSetting.openid_connect_discovery_document = '${CLOUDRON_OIDC_DISCOVERY_URL}'"

    # Cloudron OIDC is DNS mapped to an internal IP
    oidc_hostname=$(echo "$CLOUDRON_OIDC_ISSUER" | sed 's|https://\([^/]*\).*|\1|')
    gosu cloudron:cloudron bundle exec script/rails runner "SiteSetting.allowed_internal_hosts = '${oidc_hostname}|172.18.0.1'"
fi

# have to do this for domain change. this puts assets in public/assets
if ! ls /app/code/public/assets/discourse-*.js >/dev/null 2>&1; then
    echo "==> Pre-compiling assets"
    gosu cloudron:cloudron bundle exec rake assets:precompile
else
    echo "==> Skip building assets (already built)"
    gosu cloudron:cloudron bundle exec rake assets:precompile:css
fi

if [[ ! -f /app/data/.admin_setup ]]; then
    # The email is carefully chosen so that discourse always auto-picks root as the username
    echo "==> Creating administrator with username root"
    (sleep 5; echo -e "root@cloudron.local\nchangeme123\nchangeme123"; sleep 5; echo "Y") | gosu cloudron:cloudron bundle exec rake admin:create
    gosu cloudron:cloudron touch /app/data/.admin_setup
fi

# https://meta.discourse.org/t/troubleshooting-email-on-a-new-discourse-install/16326/2
echo "==> Fixing notification email"
# CLOUDRON_MAIL_FROM_DISPLAY_NAME is not supported
gosu cloudron:cloudron bundle exec script/rails runner "SiteSetting.notification_email = '${CLOUDRON_MAIL_FROM}'"

echo "==> Starting discourse"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Discourse

