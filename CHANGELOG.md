[0.1.0]
* Initial version

[0.2.0]
* Plugin support
* Doc page
* Fix svgo,pngquant gifsicle not found warnings
* Fix notification email

[0.3.0]
* Fix issue where admin page was not loading

[0.4.0]
* Fix plugin installation

[0.5.0]
* Update discourse to 2.3.2

[0.6.0]
* Fix issue where maxmind db was getting updated

[0.7.0]
* Add docs on how to change root account email

[1.0.0]
* Update discourse to 2.3.3

[1.1.0]
* Update discourse to 2.3.4

[1.1.1]
* Update Discourse to 2.3.5

[1.1.2]
* Update Discourse to 2.3.6

[1.1.3]
* Update Discourse to 2.3.7

[1.1.4]
* Update Discourse to 2.3.8

[1.2.0]
* Update Discourse to 2.4.0

[1.2.1]
* Update Discourse to 2.4.1
* Make restart faster

[1.3.0]
* Update Discourse to 2.4.2
* Use latest base image

[1.3.1]
* Update Discourse to 2.4.3

[1.3.2]
* Update Discourse to 2.4.4
* [Full changelog](https://github.com/discourse/discourse/releases/tag/v2.4.4)

[1.3.3]
* Update Discourse to 2.4.5
* [Full changelog](https://github.com/discourse/discourse/releases/tag/v2.4.5)

[1.4.0]
* Update Discourse to 2.5.0
* [Full changelog](https://github.com/discourse/discourse/releases/tag/v2.5.0)

[1.4.1]
* Update Discourse to 2.5.1
* [Full changelog](https://github.com/discourse/discourse/releases/tag/v2.5.1)

[1.4.2]
* Update Discourse to 2.5.2
* [Full changelog](https://github.com/discourse/discourse/releases/tag/v2.5.2)

[1.4.3]
* Update Discourse to 2.5.3
* [Full changelog](https://github.com/discourse/discourse/releases/tag/v2.5.3)

[1.4.4]
* Update Discourse to 2.5.4
* [Full changelog](https://github.com/discourse/discourse/releases/tag/v2.5.4)

[1.4.5]
* Update Geolite city and ASN database

[1.4.6]
* Update Discourse to 2.5.5
* [Full changelog](https://github.com/discourse/discourse/releases/tag/v2.5.5)

[1.5.0]
* Update Discourse to 2.6.0
* [Full changelog](https://github.com/discourse/discourse/releases/tag/v2.6.0)

[1.5.1]
* Update PostgreSQL client tools to v11

[1.5.2]
* Update Discourse to 2.6.1

[1.5.3]
* Update Discourse to 2.6.2

[1.5.4]
* Update Discourse to 2.6.3

[1.5.5]
* Update Discourse to 2.6.4

[1.5.6]
* Update Discourse to 2.6.5

[1.5.7]
* Update Discourse to 2.6.6

[1.5.8]
* Update Discourse to 2.6.7

[1.6.0]
* Update Discourse to 2.7.4

[1.6.1]
* Update Discourse to 2.7.5

[1.6.2]
* Update Discourse to 2.7.6

[1.6.3]
* Update Discourse to 2.7.7

[1.6.4]
* Update Discourse to 2.7.8

[1.6.5]
* Fix error when quitting irb console

[1.6.6]
* Update Discourse to 2.7.9

[1.6.7]
* Update Discourse to 2.7.10

[1.6.8]
* Update Discourse to 2.7.11

[1.6.9]
* Update Discourse to 2.7.12

[1.6.10]
* Update Discourse to 2.7.13

[1.7.0]
* Update Discourse to 2.8.0
* [Release announcement](https://blog.discourse.org/2022/01/discourse-2-8-released)

[1.7.1]
* Update Discourse to 2.8.1
* SECURITY: Onebox response timeout and size limit (#15927)

[1.7.2]
* Update Discourse to 2.8.2

[1.7.3]
* Add `logPaths` to manifest

[1.7.4]
* Fix nginx config to allow large file uploads

[1.7.5]
* Update Discourse to 2.8.3

[1.7.6]
* Update Discourse to 2.8.4

[1.7.7]
* Update Discourse to 2.8.5

[1.7.8]
* Fix incoming mail support

[1.7.9]
* Ensure pop3 email receiving is pre-setup if enabled

[1.7.10]
* Update Discourse to 3.8.6

[1.7.11]
* Update Discourse to 2.8.7

[1.7.12]
* Update Discourse to 2.8.8

[1.7.13]
* Update Discourse to 2.8.9

[1.7.14]
* Update Discourse to 2.8.11

[1.7.15]
* Update Discourse to 2.8.12

[1.7.16]
* Update Discourse to 2.8.13

[1.7.17]
* Update Discourse to 2.8.14

[1.8.0]
* Update Discourse to 3.0.1

[2.0.0]
* Update Discourse to 3.0.4
* This is a major release of discourse. Plugins will most likely break. If the app does not start, place the app in repair mode and use the [Web Terminal](https://docs.cloudron.io/apps/#web-terminal) to update plugins.

[2.0.1]
* Update Discourse to 3.0.5

[2.1.0]
* Add optional SSO through OpenID connect integration - currently only available for new app instances

[2.1.1]
* Update Discourse to 3.0.6

[2.2.0]
* Update Discourse to 3.1.0

[2.2.1]
* Update Discourse to 3.1.1

[2.2.2]
* Update Discourse to 3.1.2
* Update base image to 4.2.0

[2.2.3]
* Update Discourse to 3.1.3

[2.3.0]
* Update Discourse to 3.1.5

[2.4.0]
* Update Discourse to 3.2.0

[2.4.1]
* symlink `.local` so gems of plugins can be installed

[2.4.2]
* Update Discourse to 3.2.1

[2.4.3]
* Update Discourse to 3.2.2

[2.4.4]
* Update Discourse to 3.2.3

[2.4.5]
* Update Discourse to 3.2.4

[2.4.6]
* Update Discourse to 3.2.5

[2.5.0]
* Update Discourse to 3.3.0

[2.5.1]
* Update Discourse to 3.3.1

[2.5.2]
* Update Discourse to 3.3.2

[2.5.3]
* Update discourse to 3.3.3

[2.5.4]
* Update Discourse to 3.3.4

[2.6.0]
* Update discourse to 3.4.0

[2.6.1]
* Update discourse to 3.4.1
* [Full Changelog](https://blog.discourse.org/2025/02/unpacking-discourse-3-4/)

