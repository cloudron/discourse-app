# Discourse Cloudron App

This repository contains the Cloudron app package source for [Discourse](https://discourse.org/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=org.discourse.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id org.discourse.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd discourse-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the repos are still ok.

```
cd gitlab-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

## Notes

* discourse.conf contains the app configuration. contains basic stuff like db/redis config, domain name, email config.
* `site_setings.yml` is for site configuration. contains everything else. These things are exposed in UI, but you can decide what is visible/editable etc via this file.
