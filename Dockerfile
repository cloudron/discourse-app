FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/pkg /app/code
WORKDIR /app/code

# https://github.com/discourse/discourse_docker/blob/main/image/base/slim.Dockerfile
RUN apt-get update && \
    apt-get install -y make gcc g++ libpq-dev zlib1g-dev rbenv brotli advancecomp jhead jpegoptim libjpeg-turbo-progs optipng pngquant gifsicle && \
    rm -r /var/cache/apt /var/lib/apt/lists

# https://github.com/discourse/discourse_docker/blob/main/image/base/Dockerfile#L101
RUN npm install -g terser uglify-js pnpm@9

# We need imagemagick 7 which is not part of ubuntu for unknown reasons
RUN wget https://download.imagemagick.org/ImageMagick/download/ImageMagick.tar.gz && \
    tar xvzf ImageMagick.tar.gz && \
    cd ImageMagick-7.* && \
    ./configure && \
    make -j4 && \
    make install && \
    rm -rf ImageMagick* && \
    ldconfig /usr/local/lib

# oxipng - https://github.com/discourse/discourse_docker/blob/main/image/base/install-oxipng
ARG OXIPNG_VERSION=9.1.2
RUN mkdir /app/pkg/oxipng && \
    curl -L https://github.com/shssoichiro/oxipng/releases/download/v${OXIPNG_VERSION}/oxipng-${OXIPNG_VERSION}-x86_64-unknown-linux-musl.tar.gz | tar zxvf - --strip-components=1 -C /app/pkg/oxipng && \
    ln -s /app/pkg/oxipng/oxipng /usr/local/bin/oxipng

# for yaml-override
RUN cd /app/pkg && npm install js-yaml@3.13.1 lodash@4.17.15

# ruby: https://github.com/discourse/discourse_docker/blob/main/image/base/Dockerfile#L5
ENV RUBY_VERSION=3.3.6
ENV RUBY_MAJOR_MINOR_VERSION=3.3

RUN git clone https://github.com/rbenv/rbenv.git /usr/local/rbenv
ENV PATH /usr/local/rbenv/bin:$PATH
ENV RBENV_ROOT /home/cloudron/rbenv
RUN git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build
RUN rbenv install ${RUBY_VERSION}
ENV PATH ${RBENV_ROOT}/versions/${RUBY_VERSION}/bin:$PATH

RUN gem install bundler

ARG NODE_VERSION=20.11.0
RUN mkdir -p /usr/local/node-${NODE_VERSION} && \
    curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.xz | tar Jxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH /usr/local/node-${NODE_VERSION}/bin:$PATH

# discourse: https://github.com/discourse/discourse_docker/blob/main/image/base/release.Dockerfile
# renovate: datasource=github-tags depName=discourse/discourse versioning=semver extractVersion=^v(?<version>.+)$
ARG DISCOURSE_VERSION=3.4.1

RUN chown -R cloudron:cloudron /app/code
ENV RAILS_ENV production
USER cloudron
RUN curl -L https://github.com/discourse/discourse/archive/v${DISCOURSE_VERSION}.tar.gz | tar -xz --strip-components 1 -f - && \
    bundle config --local deployment true && \
    bundle config --local path ./vendor/bundle && \
    bundle config --local without test development && \
    bundle config --local 4 && \
    bundle install && \
    pnpm install && \
    pnpm store prune

# the code detects these values from a git repo otherwise
RUN echo -e "\$git_version='${DISCOURSE_VERSION}'\n\$full_version='${DISCOURSE_VERSION}'\n\$last_commit_date=DateTime.strptime('$(date +%s)','%s')" > /app/code/config/version.rb

# -0 sets the separator to null (instead of newline). /s will make '.' match newline. .*? means non-greedy
RUN mv /app/code/config/site_settings.yml /app/code/config/site_settings.yml.default && \
    ln -s /run/discourse/site_settings.yml /app/code/config/site_settings.yml && \
    perl -i -0p -e 's/force_https:.*?default: false/force_https:\n    default: true/ms;' /app/code/config/site_settings.yml.default

# pre-install openid-plugin - no upstream tags!
# renovate: datasource=git-refs packageName=https://github.com/discourse/discourse-openid-connect branch=master
ARG OIDC_COMMIT=f31a869611a2fad8efadd9b2504440215169a391
RUN mkdir -p /app/code/plugins/discourse-openid-connect && \
    curl -L https://github.com/discourse/discourse-openid-connect/archive/${OIDC_COMMIT}.tar.gz | tar zvxf - --strip-components 1 -C /app/code/plugins/discourse-openid-connect

# public/ - directory has files that don't change
#   assets/ - generated assets
# plugins/ - plugin code
# app/assets - app assets
RUN ln -sf /run/discourse/discourse.conf /app/code/config/discourse.conf && \
    rm -rf /app/code/log && ln -sf /run/discourse/log /app/code/log && \
    ln -s /run/discourse/tmp /app/code/tmp && \
    rm -rf /home/cloudron/.local && ln -sf /run/cloudron.local /home/cloudron/.local && \
    mv /app/code/plugins /app/code/plugins.original && ln -s /app/data/plugins /app/code/plugins

USER root

# configure nginx
COPY nginx_readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf
RUN rm /etc/nginx/sites-enabled/* && \
    sed -e 's/client_max_body_size .*/client_max_body_size 200m;/g' /app/code/config/nginx.sample.conf > /etc/nginx/sites-available/discourse && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log && \
    ln -s /etc/nginx/sites-available/discourse /etc/nginx/sites-enabled/discourse && \
    ln -s /app/data/uploads /app/code/public/uploads

# https://perldoc.perl.org/perlre.html#Modifiers
RUN perl -i -0p -e 's/upstream discourse \{.*?\}/upstream discourse { server 127.0.0.1:3000; }/ms;' \
    -e 's,.*access_log.*,access_log /dev/stdout;,;' \
    -e 's,.*error_log.*,error_log /dev/stderr info;,;' \
    -e 's/server_name.*/server_name _;/g;' \
    -e 's,/var/www/discourse,/app/code,g;' \
    -e 's,/var/nginx,/run/nginx,g;' \
    -e 's,brotli_static on,# brotli_static on,g;' \
    /etc/nginx/sites-available/discourse

# add supervisor configs
ADD supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/discourse/supervisord.log /var/log/supervisor/supervisord.log

ARG MAXMIND_ACCOUNT_ID
ARG MAXMIND_LICENSE_KEY
RUN cd /app/code/vendor/data && \
    curl -L -u "${MAXMIND_ACCOUNT_ID}:${MAXMIND_LICENSE_KEY}" https://download.maxmind.com/geoip/databases/GeoLite2-City/download?suffix=tar.gz | tar zxvf - --strip-components 1 --wildcards GeoLite2-City_*/GeoLite2-City.mmdb && \
    curl -L -u "${MAXMIND_ACCOUNT_ID}:${MAXMIND_LICENSE_KEY}" https://download.maxmind.com/geoip/databases/GeoLite2-ASN/download?suffix=tar.gz | tar zxvf - --strip-components 1 --wildcards GeoLite2-ASN_*/GeoLite2-ASN.mmdb

COPY start.sh yaml-override.js /app/pkg/

CMD [ "/app/pkg/start.sh" ]

