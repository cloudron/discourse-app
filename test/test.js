#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const TEST_TIMEOUT = 300000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = process.env.LOCATION || 'test';

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(username, password) {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/login');

        await waitForElement(By.id('login-account-name'));
        await browser.findElement(By.id('login-account-name')).sendKeys(username);
        await browser.findElement(By.id('login-account-password')).sendKeys(password);
        await browser.findElement(By.id('login-button')).click();
        await waitForElement(By.xpath('//a[contains(text(), "Admin")]'));
    }

    async function clickOidcLogin() {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/login`);

        await waitForElement(By.xpath('//button[@aria-label="with OpenID Connect"]'));
        await browser.findElement(By.xpath('//button[@aria-label="with OpenID Connect"]')).click();

        await browser.sleep(3000);

        await browser.get(`https://${app.fqdn}/u/${username}/summary`);
        await waitForElement(By.xpath(`//div[text()="${username}"]`));
    }

    async function oidcLogin(newUser = false, session = false) {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/login`);

        await waitForElement(By.xpath('//button[@aria-label="with OpenID Connect"]'));
        await browser.findElement(By.xpath('//button[@aria-label="with OpenID Connect"]')).click();

        if (!session) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        if (newUser) {
            await waitForElement(By.id('new-account-username'));
            const tmp = await browser.findElement(By.id('new-account-username'));
            expect(await tmp.getAttribute('value')).to.equal(username);

            // wait for form validation
            await browser.sleep(3000);

            await waitForElement(By.xpath('//div[@class="modal-container"]//span[text()="Sign Up"]'));
            await browser.findElement(By.xpath('//div[@class="modal-container"]//span[text()="Sign Up"]')).click();

            // wait for profile page to be created
            await browser.sleep(3000);
        }

        await browser.get(`https://${app.fqdn}/u/${username}/summary`);
        await waitForElement(By.xpath(`//div[text()="${username}"]`));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);

        await waitForElement(By.id('current-user'));
        await browser.findElement(By.id('current-user')).click();

        await waitForElement(By.id('user-menu-button-profile'));
        await browser.findElement(By.id('user-menu-button-profile')).click();

        await browser.sleep(3000);

        await waitForElement(By.xpath('//span[text()="Log Out"]'));
        await browser.findElement(By.xpath('//span[text()="Log Out"]')).click();

        await waitForElement(By.xpath('//span[text()="Log In"]'));
    }

    async function checkDashboard() {
        await browser.get('https://' + app.fqdn + '/admin');
        await waitForElement(By.xpath('//h2[text()="Version"]'));
    }

    async function checkEmail() {
        await browser.get('https://' + app.fqdn + '/admin/email');

        await waitForElement(By.xpath('//input[contains(@placeholder, "email address to test")]'));
        await browser.findElement(By.xpath('//input[contains(@placeholder, "email address to test")]')).sendKeys('test@cloudron.io');
        await browser.findElement(By.xpath('//button/span[contains(text(), "Send")]')).click(); // Test Email OR test email
        await browser.sleep(3000);

        await browser.get('https://' + app.fqdn + '/admin/email/sent');
        await waitForElement(By.xpath('//td[contains(text(), "test_message")]'));
    }

    function installPlugin() {
        execSync(`cloudron exec --app ${app.id} -- git clone https://github.com/discourse/discourse-math /app/code/plugins/discourse-math`);
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
    }

    async function checkPlugin() {
        await browser.get('https://' + app.fqdn + '/admin/plugins');
        await waitForElement(By.xpath('//div[text()="Math"]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app info', getAppInfo);

    it('can login', login.bind(null, 'root@cloudron.local', 'changeme123'));
    it('can check admin dashboard', checkDashboard);
    it('can install plugin', installPlugin);
    it('can check plugin', checkPlugin);
    it('can see mail', checkEmail);
    it('can logout', logout);

    it('can oidc login', oidcLogin.bind(null, true));
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });
    it('can login', login.bind(null, 'root@cloudron.local', 'changeme123'));
    it('can see admin dashboard', checkDashboard);
    it('can check plugin', checkPlugin);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', async function () {
        await browser.get('about:blank');

        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can see admin dashboard', checkDashboard);
    it('can check plugin', checkPlugin);

    it('move to different location', async function () {
        browser.manage().deleteAllCookies();
        await browser.get('about:blank');

        execSync(`cloudron configure --app ${app.id} --location ${LOCATION}2`, EXEC_ARGS);
    });

    it('can get app info', getAppInfo);

    it('can login', login.bind(null, 'root@cloudron.local', 'changeme123'));
    it('can see admin dashboard', checkDashboard);
    it('can check plugin', checkPlugin);
    it('can logout', logout);

    it('click oidc login', clickOidcLogin);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app for update', function () { execSync('cloudron install --appstore-id org.discourse.cloudronapp --location ' + LOCATION, EXEC_ARGS); });
    it('can get app info', getAppInfo);
    it('can login', login.bind(null, 'root@cloudron.local', 'changeme123'));
    it('can see admin dashboard', checkDashboard);
    it('can install plugin', installPlugin);
    it('can logout', logout);

    it('can oidc login', oidcLogin.bind(null, true, true));
    it('can logout', logout);

    it('can update', function () { execSync('cloudron update --app ' + LOCATION, EXEC_ARGS); });

    it('can login', login.bind(null, 'root@cloudron.local', 'changeme123'));
    it('can see admin dashboard', checkDashboard);
    it('can check email', checkEmail);
    it('can check plugin', checkPlugin);
    it('can logout', logout);

    it('can click oidc login', clickOidcLogin);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
